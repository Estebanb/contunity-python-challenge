from setuptools import setup

setup(name='contunity_challenge',
      version='0.1',
      description='Package to solve the Contunity Python Challenge',
      url='https://gitlab.com/Estebanb/contunity-python-challenge',
      author='Esteban Bosse',
      author_email='estebanbosse@gmail.com',
      license='MIT',
      packages=['contunity_challenge'],
      zip_safe=False)
