# Contunity Challenge

The challenge is to create a python script that is updating values in a XML document. (see example below)
The script should open a file dialog to hand over that XML document.

After that a new entry of type "settings" in the XML tree should be created in path "eagle/drawing/settings",
with the tag "time" and the current time in hh:mm as its value.

After that the same file is saved and overwritten with the new data.
Python version 3.7 or compatible should be used. There should be instructions to install all required dependencies (preferable in a virtual environment). To start the algorithm there should be a “start.py” that can be run from the python interpreter.

### The principal features implemented are:

1. **Unittests:** A set of 12 unittest to ensure the correct operation of the module developed to solve the challenge.
2. **Pylint:** All the code follow the code style of pylint with PEP8.
3. **Pip package:** The code ist designed to could be installed with pip.
4. **Gitlab-CI:** In Gitlab-CI there is set of Continuous Integration tests, every time that a change is pushed to de repo, 3 jobs will run, running all the unittest in two different docker images and installing the pip package to ensure that it works as expected.

# How to:
It's recommended to setup an virtualenv, install the package and use the module from the **python3.7** shell or **ipython3.7** 

## Dependences
This package only needs **python3.7** or compatible to use the module. Alternatively is necessary install the **virtualenv** if you want to use it.

### Setup virtualenv (optional):
If you want to install the module as pip package could be a good idea install it inside one virtualenv to avoid install it in your file system.
```
virtualenv -p /usr/bin/python3.7 my_virtualenv
source my_virtualenv/bin/activate
```
And to exit the virtualenv its only necessary to run:
```
deactivate
```

## How to install the package (optional):
Consider to use an virtualenv to install the package.


### Downloading the repo:
Clone repo:
```
git clone https://gitlab.com/Estebanb/contunity-python-challenge.git
```
Enter directory and install the package:
```
cd contunity_challenge && pip install .
```

### Without download the repo:
Install pip package directly from the git repo.
```
pip install git+https://gitlab.com/Estebanb/contunity-python-challenge.git
```

## How to run the executable from the common shell:
To run the binary is not necessary to install the package.

Inside the repo folder run:
```
./start.py
```
This executable accept optional argumets:
* **--settings_tree**: tree where the time will be inserted, default is: *"drawing/settings"* 
* **--file**: xml file where insert the time, default is: *"example.xml"*
* **--help**: display the available commands and they defaults

## How to use the module from the python shell:
There are two ways to use the module from the shell, the first way is importing the start.py file (this way to use the module is required by challenge) and importing the module directly.

### Importing from the start.py file:
One way to use the tool is importing from start.py, this feature it's only to follow the challenge requeriments. The recommended way to use this tool is import the module directly.

Inside the repo folder run:
```
python3
from start import XmlEditor
with XmlEditor(xml_file_path="example.xml") as editor:
    editor.insert_actual_time()
```
* Replace the *"example.xml"* file with the file to insert de time, is possible to call: `editor.get_time()` to get the time from the file.

Other alternative with only one command:
```
python3
from start import insert_actual_time_in_file
insert_actual_time_in_file(xml_file_path="example.xml")
```
* It's important to note that if you get the exception: `TimeEntryAlreadyExists` is because the file already have the time inserted,
is possible to override the actual time calling the `insert_actual_time()` with the override argument:
```
python3
from start import XmlEditor
with XmlEditor(xml_file_path="example.xml") as editor:
    editor.insert_actual_time(override=True)
```

### Importing the module directly (recommended):
```
python3
from contunity_challenge.contunity_xml_editor import XmlEditor
with XmlEditor(xml_file_path="example.xml") as editor:
    editor.insert_actual_time()
```
* Reeplace the "example.xml" file with the xml file where you want to insert the time, is possible to call: `editor.get_time()` to get the time from the file.

## How to run tests manually:
To run the tests by hand you should run this command inside the repo folder:
```
 python3 -m unittest tests/test_contunity_xml_editor.py
```

## Run pylint manually:
To run the next command you need to hawe pylint3 installed in your machine.
Pylint3 will check that all the code follow the pylint code style.
```
pylint3 contunity_challenge/*.py tests/*.py
```

## About continuous integration
This repository contains one gitlab-ci specific file: *.gitlab-ci.yml*. To activate the CI functionalities is enough to push this source a to gitlab-ci repo.

Is possible to see it working in: https://gitlab.com/Estebanb/contunity-python-challenge

### Security notice:
This module is comprometed to some XML security issues: https://docs.python.org/3/library/xml.html#xml-vulnerabilities.

The etree module was used and is vulnerable to: *billion laughs* and *quadratic blowup*.
