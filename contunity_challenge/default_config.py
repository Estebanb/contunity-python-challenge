"""
Python config file.

This files storage all the default configs for the ContunityXmlEditor
"""


SETTINGS_TREE = "drawing/settings"
XML_FILE_PATH = "example.xml"
REGEX_FIND_TIME_ENTRIES = "./"+SETTINGS_TREE+"/setting[@time]"
