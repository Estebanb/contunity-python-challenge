"""XML time editor.

This module insert the time in gived tree in a gived Contunity XML file.

How it works:

1) Open a dialog to hand over the XML document.
2) Search for settings tree.
3) Search for existant time entries
4) Decide if override it or not, if override is ton possible it raises an exception.
5) Add new entry with the tag "time" and add the current time in hh:mm format as value.
6) Save the XML document with the new entry.

"""
from time import localtime, strftime
import xml.etree.ElementTree as ET
import contunity_challenge.default_config as config


class MultipleTimeEntries(Exception):
    """
    Exception raised when there is more than one time entry in a gived Contunity config file
    """

class SettingsTreeNotFound(Exception):
    """
    Exception raised when was imposible find the settings tree in the gived Contunity config file
    """

class TimeEntryAlreadyExists(Exception):
    """
    Exception raised when the gived Contunity config file contains one or more time entries and is
    not possible to override them.
    """

class XmlEditor():
    """
    This class provides the methods to insert and get the time setting in a Continuity xml config
    file.
    Could be instanciated from the python command line and has default args: file path and settings
    tree configs.

    Attributes:
        settings_tree: the tree where settings are storaged in the Contunity XML files.

        xml_file_path: path to the Contunity XML file to edit.
    """
    def __init__(self, settings_tree=config.SETTINGS_TREE, xml_file_path=config.XML_FILE_PATH):
        self.settings_tree = settings_tree
        self.xml_file_path = xml_file_path
        self.tree = None

    def __enter__(self):
        self.read()
        return self

    def __exit__(self, *exc):
        self.write()

    def read(self):
        """
        This method read and parse the xml file.
        """
        self.tree = ET.parse(self.xml_file_path)

    def write(self):
        """
        This method write the changes to the xml original file.
        """
        self.tree.write(self.xml_file_path)


    def insert_actual_time(self, time=localtime(), override=False):
        """
        This method as default insert the actual time in the settings_tree and xml_file_path
        provided in the init, its possible to provide a diferent time with the time argument.

        This method will raise an exception if there is at least one time entry and the
        override is not activated.

        This method will delete all the existant time entries and add the new one if the
        override is activated.
        """
        if override and self._get_time_elements():
            self._delete_time_elements()
        elif self._get_time_elements():
            raise TimeEntryAlreadyExists("Time already exists in the file: {}, can not insert"\
                                         "it again".format(self.xml_file_path))

        settings = self.tree.find(self.settings_tree)
        if not settings:
            raise SettingsTreeNotFound("{} tree not found in {}, impossible insert actual time."
                                       .format(self.settings_tree, self.xml_file_path))

        actual_time = strftime("%H:%M", time)
        ET.SubElement(settings, "setting", attrib={"time":actual_time})

    def _get_time_elements(self):
        return self.tree.findall(config.REGEX_FIND_TIME_ENTRIES)

    def get_time(self):
        """
        This method get the time from the settings_tree in the xml_file_path provided in the init
        and returns None when there is no time entry point.
        Is created only for test purpouses.
        """
        time_elements = self._get_time_elements()

        if len(time_elements) > 1:
            raise MultipleTimeEntries("There is more than one time config in: {}"
                                      .format(config.REGEX_FIND_TIME_ENTRIES))
        if not time_elements:
            return None

        return time_elements[0].attrib['time']

    def _delete_time_elements(self):
        """
        This method remove all existant time elements from the xml file
        """
        settings = self.tree.find(self.settings_tree)
        time_elements = self._get_time_elements()
        for element in time_elements:
            settings.remove(element)
