#!/usr/bin/python3.7
"""
This python script insert the actual time in Contunity xml file.

Could be executed from any shell or implemented in the python command interface and provides the
ability to customize the settings tree where the time is stored and the xml file to work with.
"""

import argparse
import contunity_challenge.default_config as config
from contunity_challenge.contunity_xml_editor import XmlEditor



def create_argparse():
    """
    This function create the argparse necesary to execute this file from command line and
    provides the default configuration for file path and the settings tree.

    ArgumentDefaultsHelpFormatter its used to show default values in --help
    """
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-s", "--settings_tree",
                        help="settings xml p",
                        default=config.SETTINGS_TREE)
    parser.add_argument("-f", "--file",
                        help="xml file",
                        default=config.XML_FILE_PATH)

    args = parser.parse_args()
    return args


def insert_actual_time_in_file(xml_file_path=config.XML_FILE_PATH):
    """
    This functions insert the time in a gived Contunity XML file.

    Could be called from a python shell for fast and simple edition.
    """
    with XmlEditor(xml_file_path=xml_file_path) as xml_editor:
        xml_editor.insert_actual_time()


def main():
    """
    This main function it's only called when executing this file from command line and provides
    the ability to insert the actual time in a gived xml file.
    """
    args = create_argparse()
    with XmlEditor(args.settings_tree, args.file) as xml_editor:
        xml_editor.insert_actual_time()

if __name__ == "__main__":
    main()
