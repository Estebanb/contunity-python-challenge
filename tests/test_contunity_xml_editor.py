"""
Set of tests to ensure the right operation of the Contunity XmlEditor for the challenge.

This tests need the xml templates that are in tests/xml_test_files folder to work.
"""
import unittest
import filecmp
import xml.etree.ElementTree as ET
from tempfile import TemporaryDirectory
from time import localtime, strftime
from shutil import copy as copy_file
from contunity_challenge.contunity_xml_editor import XmlEditor, MultipleTimeEntries,\
    SettingsTreeNotFound, TimeEntryAlreadyExists

XML_FILES = 'tests/xml_test_files/'

class TestXmlEditor(unittest.TestCase):
    """
    Set of tests for the class module TestXmlEditor
    """
    def test_read_fails_when_the_file_is_not_xml(self):
        """
        Exception expected when trying to read a file with other format than XML
        """
        editor = XmlEditor(xml_file_path=XML_FILES+"no_xml_file.txt")
        self.assertRaises(ET.ParseError, editor.read)

    def test_read_fails_when_the_file_is_empty(self):
        """
        Exception expected when trying to read an empty xml file
        """
        editor = XmlEditor(xml_file_path=XML_FILES+"emtpy_file.xml")
        self.assertRaises(ET.ParseError, editor.read)

    def test_read_fails_when_the_file_doesnt_exists(self):
        """
        Exception expcted when trying to read a file that doesnt exists.
        """
        editor = XmlEditor(xml_file_path="/tmp/non_existant_file")
        self.assertRaises(FileNotFoundError, editor.read)

    def test_read_xml_file(self):
        """
        No exception expected when reading an existant file.
        """
        editor = XmlEditor(xml_file_path=XML_FILES+"xml_base_for_tests.xml")
        self.assertEqual(editor.read(), None)

    def test_write_xml_file(self):
        """
        Expect to when reading and writing the same file it doesnt change.
        This test is usefull to know if the read or write method changes the file.

        An interesting fact about this test is that when I open the file provided
        for the challenge the first time, it suffer some non-relevant changes in
        his format.
        """
        with TemporaryDirectory() as tmp_dir:
            editor = XmlEditor(xml_file_path=XML_FILES+"xml_base_for_tests.xml")
            editor.read()
            editor.xml_file_path = tmp_dir+"/bleble.xml"
            editor.write()
            self.assertTrue(filecmp.cmp(XML_FILES+"xml_base_for_tests.xml", tmp_dir+"/bleble.xml"))

    def test_get_time_from_the_xml_file(self):
        """
	Expect to get the correct time.
        The time was previously hardcoded manually in this xml file.
	"""
        with XmlEditor(xml_file_path=XML_FILES+"xml_with_time_entry.xml") as editor:
            self.assertEqual("21:33", editor.get_time())

    def test_insert_time_in_the_xml_file(self):
        """
        Expect to insert the time in the xml file.
        To ensure the right write, after write is readed and compared with the original
        """
        actual_time = localtime()
        with TemporaryDirectory() as tmp_dir:
            copy_file(XML_FILES+"xml_base_for_tests.xml", tmp_dir+"/xml_tests.xml")
            with XmlEditor(xml_file_path=tmp_dir+"/xml_tests.xml") as editor:
                editor.insert_actual_time(actual_time)
                file_time = editor.get_time()
                self.assertEqual(strftime("%H:%M", actual_time), file_time)

    def test_insert_time_when_time_already_exists_and_override_activated(self):
        """
        Expect to insert a new time entry when there is an old time entry.
        To ensure the write override, the test read the original value in the file,
        write a new one an check that the old is different from the new.

        If we run this test at the same hour that is hardcoded in the file will fail.
        """
        actual_time = localtime()
        with TemporaryDirectory() as tmp_dir:
            copy_file(XML_FILES+"xml_with_time_entry.xml", tmp_dir+"/xml_tests.xml")
            with XmlEditor(xml_file_path=tmp_dir+"/xml_tests.xml") as editor:
                old_time = editor.get_time()
                editor.insert_actual_time(actual_time, override=True)
                file_time = editor.get_time()
                self.assertNotEqual(strftime("%H:%M", actual_time), old_time)
                self.assertEqual(strftime("%H:%M", actual_time), file_time)

    def test_insert_time_when_time_already_exists_without_override(self):
        """
        Exception expected when trying to insert a new time entry and the xml file
        already have an old time with the override deactivated.
        """
        actual_time = localtime()
        with TemporaryDirectory() as tmp_dir:
            copy_file(XML_FILES+"xml_with_time_entry.xml", tmp_dir+"/xml_tests.xml")
            with XmlEditor(xml_file_path=tmp_dir+"/xml_tests.xml") as editor:
                self.assertRaises(TimeEntryAlreadyExists, editor.insert_actual_time,
                                  actual_time, override=False)
                file_time = editor.get_time()
                self.assertNotEqual(strftime("%H:%M", actual_time), file_time)

    def test_insert_time_when_the_tree_doesnt_exsists_in_xml_file(self):
        """
        Exception expected when trying to insert time in a file that doesnt contain the
        drawing/settings tree
        """
        with XmlEditor(xml_file_path=XML_FILES+"xml_without_settings_tree.xml") as editor:
            self.assertRaises(SettingsTreeNotFound, editor.insert_actual_time)

    def test_read_raises_exception_when_multiple_time_entries_in_xml_file(self):
        """
        Exception expected when getting time from a xml file with multiple time entries
        """
        with XmlEditor(xml_file_path=XML_FILES+"xml_with_duplicated_time_entries.xml") as editor:
            self.assertRaises(MultipleTimeEntries, editor.get_time)

    def test_get_time_when_there_is_no_time_entries_in_xml_file(self):
        """
        Expect to get_time() return None when there is not time entries in the xml file
        """
        with XmlEditor(xml_file_path=XML_FILES+"xml_base_for_tests.xml") as editor:
            self.assertIsNone(editor.get_time())



if __name__ == '__main__':
    unittest.main()
